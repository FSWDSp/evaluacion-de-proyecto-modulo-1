import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Elemento } from './../models/elemento.model';

@Component({
  selector: 'app-elemento',
  templateUrl: './elemento.component.html',
  styleUrls: ['./elemento.component.css']
})
export class ElementoComponent implements OnInit {
  @Input() elemento: Elemento;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { }

  ngOnInit() {
  }

}
