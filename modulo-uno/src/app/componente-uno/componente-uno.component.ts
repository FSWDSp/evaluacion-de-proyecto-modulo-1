import { Component, OnInit } from '@angular/core';
import { Elemento } from './../models/elemento.model';

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})
export class ComponenteUnoComponent implements OnInit {
  elementos: Elemento[];

  constructor() {
  	this.elementos = [];
  }

  ngOnInit() {
  }

  agregar(nombre:string, descripcion:string):boolean {
  	this.elementos.push(new Elemento(nombre, descripcion));
  	return false;
  }

}
